'''
This implementation is adapted from github repo:
https://github.com/wenet-e2e/wespeaker
'''

import math


class BaseClass:
    '''
    Base Class for learning rate scheduler
    '''

    def __init__(self,
                 optimizer,
                 num_epochs,
                 epoch_iter,
                 initial_lr,
                 final_lr,
                 warm_up_epoch=6,
                 scale_ratio=1.0,
                 warm_from_zero=False):
        '''
        warm_up_epoch: the first warm_up_epoch is the multiprocess warm-up stage
        scale_ratio: multiplied to the current lr in the multiprocess training
        process
        '''
        self.optimizer = optimizer
        self.max_iter = num_epochs * epoch_iter
        self.initial_lr = initial_lr
        self.final_lr = final_lr
        self.scale_ratio = scale_ratio
        self.current_iter = 0
        self.warm_up_iter = warm_up_epoch * epoch_iter
        self.warm_from_zero = warm_from_zero

    def get_multi_process_coeff(self):
        lr_coeff = 1.0 * self.scale_ratio
        if self.current_iter < self.warm_up_iter:
            if self.warm_from_zero:
                lr_coeff = self.scale_ratio * self.current_iter / self.warm_up_iter
            elif self.scale_ratio > 1:
                lr_coeff = (self.scale_ratio -
                            1) * self.current_iter / self.warm_up_iter + 1.0

        return lr_coeff

    def get_current_lr(self):
        '''
        This function should be implemented in the child class
        '''
        return 0.0

    def get_lr(self):
        return self.optimizer.param_groups[0]['lr']

    def set_lr(self):
        current_lr = self.get_current_lr()
        for param_group in self.optimizer.param_groups:
            param_group['lr'] = current_lr

    def step(self, current_iter=None):
        if current_iter is not None:
            self.current_iter = current_iter

        self.set_lr()
        self.current_iter += 1

    def step_return_lr(self, current_iter=None):
        if current_iter is not None:
            self.current_iter = current_iter

        current_lr = self.get_current_lr()
        self.current_iter += 1

        return current_lr


class ExponentialDecrease(BaseClass):

    def __init__(self,
                 optimizer,
                 num_epochs,
                 epoch_iter,
                 initial_lr,
                 final_lr,
                 warm_up_epoch=6,
                 scale_ratio=1.0,
                 warm_from_zero=False):
        super().__init__(optimizer, num_epochs, epoch_iter, initial_lr,
                         final_lr, warm_up_epoch, scale_ratio, warm_from_zero)

    def get_current_lr(self):
        lr_coeff = self.get_multi_process_coeff()
        current_lr = lr_coeff * self.initial_lr * math.exp(
            (self.current_iter / self.max_iter) *
            math.log(self.final_lr / self.initial_lr))
        return current_lr



class MarginScheduler:

    def __init__(self,
                 loss,
                 epoch_iter,
                 increase_start_epoch,
                 fix_start_epoch,
                 initial_margin,
                 final_margin,
                 update_margin,
                 increase_type='exp'):
        '''
        The margin is fixed as initial_margin before increase_start_epoch,
        between increase_start_epoch and fix_start_epoch, the margin is
        exponentially increasing from initial_margin to final_margin
        after fix_start_epoch, the margin is fixed as final_margin.
        '''
        self.loss = loss
        self.increase_start_iter = increase_start_epoch * epoch_iter
        self.fix_start_iter = fix_start_epoch * epoch_iter
        self.initial_margin = initial_margin
        self.final_margin = final_margin
        self.increase_type = increase_type

        self.fix_already = False
        self.current_iter = 0
        self.update_margin = update_margin 
        self.increase_iter = self.fix_start_iter - self.increase_start_iter

        self.init_margin()

    def init_margin(self):
        self.loss.update(margin=self.initial_margin)

    def get_increase_margin(self):
        initial_val = 1.0
        final_val = 1e-3

        current_iter = self.current_iter - self.increase_start_iter

        if self.increase_type == 'exp':  # exponentially increase the margin
            ratio = 1.0 - math.exp((current_iter / self.increase_iter) *
                math.log(final_val / (initial_val + 1e-6))) * initial_val
        else:  # linearly increase the margin
            ratio = 1.0 * current_iter / self.increase_iter
        return self.initial_margin + (self.final_margin - self.initial_margin) * ratio

    def step(self, current_iter=None):
        if not self.update_margin or self.fix_already:
            return

        if current_iter is not None:
            self.current_iter = current_iter

        if self.current_iter >= self.fix_start_iter:
            self.fix_already = True
            self.loss.update(margin=self.final_margin)
        elif self.current_iter >= self.increase_start_iter:
            self.loss.update(margin=self.get_increase_margin())

        self.current_iter += 1

    def get_margin(self):
        try:
            margin = self.loss.m
        except Exception:
            margin = 0.0

        return margin
